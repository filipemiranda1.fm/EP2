/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import model.Employee;

/**
 *
 * @author cienth
 */
public class EmployeeController {
    
    private List<Employee> listEmployee;
    EmployeeController employeeControlled;
    
    
    public EmployeeController() throws IOException{
     
        loadEmployees();
        
    }
    
    private void loadEmployees(){
        listEmployee = new ArrayList<>();
        
        try{
            InputStream is = new FileInputStream("db/employees.txt");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String name = br.readLine();
            String login = br.readLine();
            String pwd = br.readLine();

            while (login != null && pwd != null) {

                Employee employee = new Employee(name, login, pwd);
                listEmployee.add(employee);

                name = br.readLine();
                login = br.readLine();
                pwd = br.readLine();

            }

            br.close();
        }catch(IOException e){
            
        }
    }
    
    public Employee validateLogin(String login, String password){
        
        for (Employee e : listEmployee){
            if (login.equals(e.getLogin()) && password.equals(e.getPassword())) return e;
            }
          return null;
        
    }

    public List<Employee> getListEmployee() {
        return listEmployee;
    }

    public void setListEmployee(List<Employee> listEmployee) {
        this.listEmployee = listEmployee;
    }
        
    public void addToList(Employee e){
        listEmployee.add(e);
    }
    
    public void removeFromList(Employee e){
        listEmployee.remove(e);
    }
    
}
