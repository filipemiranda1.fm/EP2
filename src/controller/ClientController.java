/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;import java.util.Collections;
;
import model.Client;

/**
 *
 * @author cienth
 */
public class ClientController {
    
    private ArrayList<Client> listClients;
    
    public ClientController() {
        loadClients();
    }
    
    private void loadClients(){
        
        listClients = new ArrayList<>();
        
        try{
   
            InputStream is = new FileInputStream("db/clients.txt");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String name = br.readLine();
            String phone = br.readLine();

            while (name != null && phone != null) {

                Client client = new Client(name, phone);
                listClients.add(client);

                name = br.readLine();
                phone = br.readLine();

            }

            br.close();
        }catch(IOException e){
            
        }

    }
    
    private void saveClients(){
        
        try{
        
            OutputStream os = new FileOutputStream("db/clients.txt");
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            for (Client c : listClients){
                bw.write(c.getName());
                bw.newLine();
                bw.write(c.getPhone());
                bw.newLine();
            }
            
       
            bw.close();

        }catch(IOException e){
            
        }
    }

    public ArrayList<Client> getListClients() {
        return listClients;
    }

    public void setListClients(ArrayList<Client> listClients) {
        this.listClients = listClients;
    }
    
    public void addToList(Client c){
        listClients.add(c);
        this.saveClients();
    }
    
    public void removeFromList(Client c){
        listClients.remove(c);
        this.saveClients();
    }
    
    public Client searchClient(String name){
        
        for (Client c : listClients){
            if(name.equals(c.getName())) return c;
        }
        return null;
    }
    
    public void editClient(Client c0, Client c1){
       
       c0.setName(c1.getName());
       c0.setPhone(c1.getPhone());
       
       this.saveClients();
        
    }
    
    
}
