/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Employee;
import view.EmployeeLogin;
import view.EmployeeWorkSystem;

/**
 *
 * @author cienth
 */
public class AppController {
    
    public static void main(String[] args) {
        EmployeeLogin el = new EmployeeLogin();
        el.setVisible(true);
    }
    
    public static void mainPage(Employee e) {
        
        EmployeeWorkSystem ews = new EmployeeWorkSystem(e);
        ews.setVisible(true);
    }
    
}
