/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import model.Client;
import model.Dessert;
import model.Dish;
import model.Drink;
import model.Product;


/**
 *
 * @author cienth
 */
public class ProductController {
    
    private ArrayList<Product> dishList;
    private ArrayList<Product> drinkList;
    private ArrayList<Product> dessertList;
    
 
    public ProductController(){
        
        this.loadProducts();
    }
    
    private void loadProducts(){
        
        this.loadDesserts();
        this.loadDrinks();
        this.loadDishes();
        

    }
    
    public void saveProducts(){
        this.saveDishes();
        this.saveDrinks();
        this.saveDesserts();
        
        
        
    }
    
    public void addToDishList(Product d){
        this.dishList.add(d);
        this.saveDishes();
        this.loadDishes();
    }
    
    public void removeFromDishList(Product d){
        this.dishList.remove(d);
        this.saveDishes();
        this.loadDishes();
    }
    
    public void addToDrinkList(Product d){
        this.drinkList.add(d);
        this.saveDrinks();
        this.loadDrinks();
    }
    
    public void removeFromDrinkList(Product d){
        this.dishList.remove(d);
        this.saveDrinks();
        this.loadDrinks();
    }
    
    public void addToDessertList(Product d){
        this.dessertList.add(d);
        this.saveDesserts();
        this.loadDesserts();
    }
    
    public void removeFromDessertList(Product d){
        this.dessertList.remove(d);
        this.saveDesserts();
        this.loadDesserts();
    }
    
    public void saveDrinks(){

            
                try{
        
                    OutputStream os = new FileOutputStream("db/products/drinks.txt");
                    OutputStreamWriter osw = new OutputStreamWriter(os);
                    BufferedWriter bw = new BufferedWriter(osw);

                    for (Product d : drinkList){
                        bw.write(d.getName());
                        bw.newLine();
                        bw.write(d.getPrice());
                        bw.newLine();
                        bw.write(d.getAmmount().toString());
                        bw.newLine();
                    }


                    bw.close();

            }catch(IOException e){
                
                   
            
            }
            
            
            
            
        }
    
    public void loadDrinks(){
        drinkList=  new ArrayList<>();
        
        
        try{
   
            InputStream is = new FileInputStream("db/products/drinks.txt");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String name = br.readLine();
            String price = br.readLine();
            String ammount = br.readLine();

            while (name != null && price != null) {

                Product product = new Drink(name, price, Integer.parseInt(ammount));
                drinkList.add(product);

                name = br.readLine();
                price = br.readLine();
                ammount = br.readLine();

            }

            br.close();
        }catch(IOException e){
            
        }
    }
    
    public void saveDishes(){

            
                try{
        
                    OutputStream os = new FileOutputStream("db/products/dishes.txt");
                    OutputStreamWriter osw = new OutputStreamWriter(os);
                    BufferedWriter bw = new BufferedWriter(osw);

                    for (Product d : dishList){
                        bw.write(d.getName());
                        bw.newLine();
                        bw.write(d.getPrice());
                        bw.newLine();
                        bw.write(d.getAmmount().toString());
                        bw.newLine();
                    }


                    bw.close();

            }catch(IOException e){
            
            }
            
            
            
            
        }
    
    public void loadDishes(){
        dishList = new ArrayList<>();
        
        try{
   
            InputStream is = new FileInputStream("db/products/dishes.txt");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String name = br.readLine();
            String price = br.readLine();
            String ammount = br.readLine();

            while (name != null && price != null) {

                Product product = new Dish(name, price, Integer.parseInt(ammount));
                dishList.add(product);

                name = br.readLine();
                price = br.readLine();
                ammount = br.readLine();

            }

            br.close();
        }catch(IOException e){
            
        }
    }
    
    public void saveDesserts(){

            
                try{
        
                    OutputStream os = new FileOutputStream("db/products/desserts.txt");
                    OutputStreamWriter osw = new OutputStreamWriter(os);
                    BufferedWriter bw = new BufferedWriter(osw);

                    for (Product d : dessertList){
                        bw.write(d.getName());
                        bw.newLine();
                        bw.write(d.getPrice());
                        bw.newLine();
                        bw.write(d.getAmmount().toString());
                        bw.newLine();
                    }


                    bw.close();

            }catch(IOException e){
            
            }
            
            
            
            
        }
    
    public void loadDesserts(){
  
        dessertList = new ArrayList<>();
        try{
   
            InputStream is = new FileInputStream("db/products/desserts.txt");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String name = br.readLine();
            String price = br.readLine();
            String ammount = br.readLine();

            while (name != null && price != null) {

                Product product = new Dessert(name, price, Integer.parseInt(ammount));
                dessertList.add(product);

                name = br.readLine();
                price = br.readLine();
                ammount = br.readLine();

            }

            br.close();
        }catch(IOException e){
            
        }
    }

    public ArrayList<Product> getDishList() {
        return dishList;
    }

    public void setDishList(ArrayList<Product> dishList) {
        this.dishList = dishList;
    }

    public ArrayList<Product> getDrinkList() {
        return drinkList;
    }

    public void setDrinkList(ArrayList<Product> drinkList) {
        this.drinkList = drinkList;
    }

    public ArrayList<Product> getDessertList() {
        return dessertList;
    }

    public void setDessertList(ArrayList<Product> dessertList) {
        this.dessertList = dessertList;
    }
    
    public Product searchProduct(String name){
        
        for (Product p : dishList){
            if(name.equals(p.getName())) return p; 
        }
        for (Product p : drinkList){
            if(name.equals(p.getName())) return p;
        }
        for (Product p : dessertList){
            if(name.equals(p.getName())) return p;
        }
        return null;
    }
    
        
    
    
    
    
    }
    

